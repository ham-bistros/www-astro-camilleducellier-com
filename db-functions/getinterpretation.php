<?php

header('Content-Type: text/html; charset=UTF-8');

require "../../ressources/connect.php";
$db = new PDO("mysql:host=".$HOST.";dbname=".$DB, $USER, $PASSWORD);

if ($db->connect_error) {
    exit('Problème de connexion à la base de données');
}

$statement = $db->prepare('SELECT * FROM interpretations WHERE planet_id like :planet AND zodiac_id like :zodiac AND house_id like :house');
$statement->execute(
    [
    'planet' => $_POST["planet"],
    'zodiac' => $_POST["zodiac"],
    'house' => $_POST["house"]
    ]
);

$interpretations = $statement->fetchAll(PDO::FETCH_NAMED);

$statement = $db->prepare("SELECT meaning  FROM planets WHERE id = :id");
$statement->execute(
    [
    'id' => $_POST["planet"],
    ]
);
$planet = $statement->fetchAll(PDO::FETCH_NAMED)[0]["meaning"];

$statement = $db->prepare("SELECT meaning FROM zodiacs WHERE id = :id");
$statement->execute(
    [
    'id' => $_POST["zodiac"],
    ]
);
$zodiac = $statement->fetchAll(PDO::FETCH_NAMED)[0]["meaning"];

$statement = $db->prepare("SELECT meaning FROM houses WHERE id = :id");
$statement->execute(
    [
    'id' => $_POST["house"],
    ]
);
$house = $statement->fetchAll(PDO::FETCH_NAMED)[0]["meaning"];

?>

<p id="meanings">
    <?php
if ($planet) {
    echo $planet;
} else {
    echo "---";
}

echo "<br/>";
if ($zodiac) {
    echo $zodiac;
} else {
    echo "---";
}

echo "<br/>";
if ($house) {
    echo $house;
} else {
    echo "---";
}

?>
</p>

<table id="interpretations">
    <tr>
	<th>ID</th>
	<th>Interprétation</th>
	<th>Supprimer</th>
    </tr>

<?php foreach ($interpretations as $phrase): ?>
    <tr>
	<td class="inter_id"><?php echo $phrase["id"] ?></td>
	<td><?php echo utf8_encode($phrase["text"]) ?></td>
	<td><button type="button" onclick="deleteInfo(event, this)">❌</button></td>
    </tr>
<?php endforeach ?>

</table>

<?php if ($_POST["is_complete"] == "true"): ?>
<form id="interpretation">
    <h3>Ajouter une interprétation : </h3>
	<input id="input" type="text" name="interpretation">
	<input type="submit" value="Envoyer la phrase" onclick="postInfo(event, 'table')">
    </form>
<?php endif ?>
