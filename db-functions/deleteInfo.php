<?php
/**
 * C'est un fichier sympa
 * */
header('Content-Type: text/html; charset=UTF-8');

require "../../ressources/connect.php";
$db = new PDO("mysql:host=".$HOST.";dbname=".$DB, $USER, $PASSWORD);

if ($db->connect_error) {
    exit('Problème de connexion à la base de données');
}

$statement = $db->prepare("DELETE FROM interpretations WHERE id = :id");
$statement->execute(['id' => $_POST['id']]);
