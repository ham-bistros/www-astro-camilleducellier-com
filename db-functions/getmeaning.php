<?php

header('Content-Type: text/html; charset=UTF-8');

require "../../ressources/connect.php";
$db = new PDO("mysql:host=" . $HOST . ";dbname=" . $DB, $USER, $PASSWORD);

if ($db->connect_error) {
    exit('Problème de connexion à la base de données');
}

$statement = $db->prepare("SELECT * FROM planets WHERE id = :id");
$statement->execute(
    [
        'id' => $_POST["planet"],
    ]
);
$planet = $statement->fetchAll(PDO::FETCH_NAMED)[0];

$statement = $db->prepare("SELECT * FROM zodiacs WHERE id = :id");
$statement->execute(
    [
        'id' => $_POST["zodiac"],
    ]
);
$zodiac = $statement->fetchAll(PDO::FETCH_NAMED)[0];

$statement = $db->prepare("SELECT * FROM houses WHERE id = :id");
$statement->execute(
    [
        'id' => $_POST["house"],
    ]
);
$house = $statement->fetchAll(PDO::FETCH_NAMED)[0];

?>

<!-- La réponse injectée dans la page #astro -->
<p id="meaning">
    <span><?= $planet["meaning"] ?></span>
    <span><?= $zodiac["meaning"] ?></span>
    <span><?= $house["meaning"] ?></span>
</p>

<button id="next-eclipse" onclick="nextEclipse()" title="Écrire une interprétation">🖋</button>

<div id="resume">
    <!-- Les images et le premier résumé -->
    <div class="visuals">
        <?php
        foreach (["planet", "zodiac", "house"] as $key => $value) {
            echo '<img src="/user/themes/astro/images/visuals/' . $value . '/' . $$value["id"] . '.png" />';
        }
?>
    </div>

    <p><span class="texte-tirage"><?= $planet["name"]?></span> <span class="texte-tirage">en <?= $zodiac["name"]?></span> <span class="texte-tirage">Maison <?= $house["id"]?></span></p>

    <button onclick="eclipse('meaning')" title="Lire l'interprétation du tirage">👁</button>
    <?php
$stmt = $db->prepare("SELECT id, text FROM interpretations WHERE planet_id = :planet_id AND zodiac_id = :zodiac_id AND house_id = :house_id");

$stmt->execute(
    [
    "planet_id" => $planet["id"],
    "zodiac_id" => $zodiac["id"],
    "house_id" => $house["id"]
    ]
);

$interpretations = $stmt->fetchAll(PDO::FETCH_NAMED);

?>

</div>

<div class="hidden">
    <p id="planet_id"><?= $planet["id"] ?></p>
    <p id="zodiac_id"><?= $zodiac["id"] ?></p>
    <p id="house_id"><?= $house["id"] ?></p>
</div>

<div id="inter">

    <div id="sumup">
        <p class="tirage-names">
            <span><?= $planet["name"] ?></span>
            <span><?= $zodiac["name"] ?></span>
            <span><?= $house["name"] ?></span>
        </p>

        <p class="tirage-meaning">
            <span><?= $planet["meaning"] ?></span>
            <span><?= $zodiac["meaning"] ?></span>
            <span><?= $house["meaning"] ?></span>
        </p>
    </div>

    <form id="interpretation">
        <p>Ajoutez votre propre interpretation :</p>
        <input  type="text" name="interpretation">
        <input id="input" type="submit" value="Envoyer la phrase" onclick="postInfo(event, 'ul')">
    </form>



    <?php if (!empty($interpretations)) : ?>
        <button id="commu" onclick="display(document.getElementById('interpretations'))" title="Écrire une interprétation">🌐</button>
        <p>Les interprétations de ce tirage : par la communauté </p>
        <ul id="interpretations">

            <?php foreach ($interpretations as $phrase) : ?>
                <li><?php echo $phrase["text"] ?></li>
            <?php endforeach ?>
        </ul>

    <?php else: ?>
        <ul id="interpretations">
        </ul>

    <?php endif ?>

</div>
