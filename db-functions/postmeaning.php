<?php

header('Content-Type: text/html; charset=UTF-8');

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

require "../../ressources/connect.php";
$db = new PDO("mysql:host=".$HOST.";dbname=".$DB, $USER, $PASSWORD);

if ($db->connect_error) {
    exit('Problème de connexion à la base de données');
}

$statement = $db->prepare("INSERT INTO interpretations (planet_id, zodiac_id, house_id, text) VALUES (:planet_id, :zodiac_id, :house_id, :interpretation)");
$statement->execute(
    [
    "planet_id" => utf8_encode($_POST['planet']),
    "zodiac_id" => utf8_encode($_POST['zodiac']),
    "house_id" => utf8_encode($_POST['house']),
    "interpretation" => utf8_encode($_POST['interpretation'])
    ]
);
