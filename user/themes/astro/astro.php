<?php

namespace Grav\Theme;

use Grav\Common\Theme;
use Grav\Common\Grav;

class Astro extends Theme
{
    // Add images to twig template paths to allow inclusion of SVG files (Quark theme thing)
    // public function onTwigLoader(): void
    // {
    //     $theme_path = Grav::instance()['locator']->findResource('theme://');
    //     $this->grav['twig']->addPath($theme_path . '/images', 'images');
    // }

}
