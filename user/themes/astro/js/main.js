// Extends Arrays to add a random method
Array.prototype.rand = function () {
  return this[Math.floor(Math.random() * this.length)];
};

function fadeOut(el, duration) {
  const anim = [{ opacity: "1" }, { opacity: "0" }];
  el.animate(anim, {
    duration: duration,
    easing: "ease-out",
    fill: "both",
  });

  return Promise.all(el.getAnimations().map((animation) => animation.finished));
}

function fadeAndDie(el, duration) {
  fadeOut(el, duration).then(() => {
    el.style.display = "none";
  });
}

function randomInt(max) {
  // random integer between 1 and <max>
  return Math.floor(Math.random() * max) + 1;
}

function tirage(el) {
  hide(el);

  let xhttp;

  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("response").innerHTML = this.responseText;
    }
  };

  xhttp.open("POST", "/db-functions/getmeaning.php", true);
  xhttp.setRequestHeader(
    "Content-type",
    "application/x-www-form-urlencoded;charset=UTF-8",
  );

  // SEEDED
  // xhttp.send(`planet=1&zodiac=1&house=1&is_complete=true`);

  // ALEATOIRE
  xhttp.send(
    `planet=${randomInt(12)}
     &zodiac=${randomInt(12)}
     &house=${randomInt(12)}
     &is_complete=true`,
  );
}

function nextEclipse() {
  eclipse("interpretation").then(() => {
    document.getElementById("inter").style.display = "block";

    setTimeout(() => {
      document.getElementById("astro").classList.add("baleze");
    }, 2000);
  });
}

function eclipse(type) {
  let easing;
  const meaning = document.getElementById("meaning");
  const nextEclipse = document.getElementById("next-eclipse");

  if (type == "meaning") {
    document.getElementById("resume").style.transform =
      "scale(.5) translateY(-100%)";
    meaning.classList.add("display");

    const button = document
      .getElementById("resume")
      .getElementsByTagName("button")[0];
    fadeOut(button, 1000).then(() => {
      button.style.visibility = "hidden";
    });

    setTimeout(() => {
      nextEclipse.style.opacity = "1";
    }, 4000);

    easing = "ease-in";
  } else {
    easing = "ease-out";
    fadeAndDie(document.getElementById("resume"), 1000);
    fadeAndDie(meaning, 2000);
    fadeAndDie(nextEclipse, 1000);
  }

  return Promise.all(
    document
      .getElementById("cercle")
      .getAnimations()
      .map((animation) => animation.finished),
  );
}

function goAstro(delayed) {
  const pageAstro = document.getElementById("astro");
  pageAstro.classList.add("go-astro", "active");

  document.body.classList.add("fond-astro");

  if (delayed) {
    pageAstro.classList.add("delayed");
  }

  display(document.getElementById("intro"));
}

function offAstro(depart, arrivee) {
  const response = document.getElementById("response");
  if (response.innerHTML) {
    response.innerHTML = "";
  }

  document.getElementById(depart).classList.remove("active");
  document.getElementById(arrivee).classList.add("active");

  document.getElementById("astro").classList.remove("baleze");
  document.getElementById("astro").classList.remove("go-astro", "active");
  hide(document.getElementById("intro"));
}

function hide(el) {
  el.style.display = "none";
}

function display(el) {
  el.style.display = "block";

  setTimeout(() => {
    el.classList.add("display");
  }, 500);
}

function getInfo(event, is_meaning) {
  event.preventDefault();
  const planet = document.querySelector("select").value;
  const zodiac = document.querySelector('select[name="zodiacs"]').value;
  const house = document.querySelector('select[name="houses"]').value;

  let xhttp;

  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("response").innerHTML = this.responseText;
    }
  };

  const action = is_meaning
    ? "/db-functions/getmeaning.php"
    : "/db-functions/getinterpretation.php";

  const is_complete =
    planet != "%" && zodiac != "%" && house != "%" ? true : false;

  xhttp.open("POST", action, true);
  xhttp.setRequestHeader(
    "Content-type",
    "application/x-www-form-urlencoded;charset=UTF-8",
  );
  xhttp.send(
    `planet=${planet}&zodiac=${zodiac}&house=${house}&is_complete=${is_complete}`,
  );
}

function postInfo(event, content) {
  event.preventDefault();

  const planet = document.getElementById("planet_id").innerHTML;
  const zodiac = document.getElementById("zodiac_id").innerHTML;
  const house = document.getElementById("house_id").innerHTML;

  const interp = document.querySelector('input[name="interpretation"]').value;

  let xhttp;

  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      if (content == "ul") {
        const li = document.createElement("li");
        const ul = document.getElementById("interpretations");

        if (document.getElementById("padinter")) {
          document.getElementById("padinter").remove();

          const para = document.createElement("p");
          para.innerHTML = "Les interprétations possibles de ce tirage :";
          ul.appendChild(para);
        }

        li.innerHTML = interp;
        ul.appendChild(li);
      } else {
        getInfo(event, false);
      }
    }
  };

  xhttp.open("POST", `/db-functions/postmeaning.php`, true);
  xhttp.setRequestHeader(
    "Content-type",
    "application/x-www-form-urlencoded;charset=UTF-8",
  );
  xhttp.send(
    `planet=${planet}&zodiac=${zodiac}&house=${house}&interpretation=${interp}`,
  );
}

function deleteInfo(event, el) {
  id = el.parentElement.parentElement.querySelector(".inter_id").innerHTML;

  let xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      getInfo(event, false);
    }
  };

  xhttp.open("POST", `/db-functions/deleteInfo.php`, true);
  xhttp.setRequestHeader(
    "Content-type",
    "application/x-www-form-urlencoded;charset=UTF-8",
  );
  xhttp.send(`id=${id}`);
}
