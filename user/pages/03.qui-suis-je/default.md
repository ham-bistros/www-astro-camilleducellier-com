---
title: "Qui suis-je ?"
media_order: carte-astrale.png
image: carte-astrale.png
visible: false
---

Je m'appelle Camille Ducellier et je pratique l'astrologie humaniste appliquée.

L'astrologie est l'une de mes activités, car je suis aussi artiste, auteure, et réalisatrice de films documentaires.

Depuis 2010, mon travail artistique met en lumière les cultures sorcières et les pratiques de celleux qui vivent en marge. Toujours entre-deux, j'aime faire des ponts joyeux entre une approche spirituelle et une dimension politique.

L'astrologie, comme bien des domaines, a trop longtemps été étudiée et utilisée dans une approche androcentrée et binaire. J'espère contribier à une relecture féministe de celle-ci.

J'étudie l'astrologie humaniste appliquée depuis 10 ans, au sein de l'école fondée par Sylvie Lafuente Sampietro.

Je considère l'astrologie a la fois comme un outil de connaissance de soi, un mode d'emploi concret de nos fonctionnements, mais aussi comme une langue étrangère et mystérieuse à décrypter. Ainsi, j'aborde cet outil comme une poésie, à la croisée de la géométrie, de la mythologie et de la psychanalyse.

Pour découvrir mes activités artistiques : [www.camilleducellier.com](https://www.camilleducellier.com)

