---
title: Consultations
body_classes: consultations
visible: false
---

Je propose deux types de consultation concernant uniquement la lecture de votre thème astral :

## Option 1 - Le thème entier</br>
Durée : 1h30</br>
<abbr title="Les prix doux sont des tarifs accessibles aux personnes en difficulté, sans justificatif.">70 euros*</abbr> - 80 euros

Une consultation pour mieux se connaître dans les divers secteurs de la vie, pour découvrir ses ressources, ses zones de confort et d'inconfort, ses schémas répétitifs, ses talents et donner du sens à son chemin singulier.

## Option 2 - Une question précise</br>
Durée: 45min</br>
<abbr title="Les prix doux sont des tarifs accessibles aux personnes en difficulté, sans justificatif.">45 euros*</abbr> - 55 euros

Une consultation pour décrypter un secteur en particulier (travail, création, fratrie, lieu de vie, santé mentale, relations amoureuses, etc.) à travers une question ou simplement le choix d'un secteur.