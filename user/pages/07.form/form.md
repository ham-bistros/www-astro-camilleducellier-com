---
title: Form
form:
  name: contact-form
  id: contact-form
  inline_errors: true
  fields:
    first:
      label: Prénom
      display_label: true
      placeholder: Prénom
      autofocus: "on"
      autocomplete: "on"
      type: text
      validate:
        required: true
    name:
      label: Nom
      display_label: true
      placeholder: Nom
      autocomplete: "on"
      type: text
      validate:
        required: true
    email:
      label: Email
      display_label: true
      placeholder: example@email.com
      type: email
      autocomplete: "on"
      validate:
        required: true
    message:
      label: Message
      display_label: true
      placeholder: "Entrer votre message ici"
      type: textarea
      minlength: 10
      maxlength: 255
      validate:
        required: true
    consultation:
      label: "Type de consultation"
      placeholder: Consultation
      id: consult
      display_label: true
      type: select
      name: "type-consultation"
      title: "type de consultation"
      options:
        complete: "Thème entier"
        precise: "Question précise"
  buttons:
    submit:
      type: submit
      value: Envoyer
      label: Envoyer

  process:
    email:
      - from: "{{ form.value.email|e }}"
        to: "{{ config.plugins.email.to }}"
        subject: "[Demande consultation {{ form.value.consultation|e }}] {{ form.value.first|e }} {{ form.value.name|e }}"
        body: "{% include 'forms/data.html.twig' %}"

      - from: "{{ config.plugins.email.from }}"
        to: "{{ form.value.email|e }}"
        subject: "Demande de consultation avec Camille Ducellier"
        template: "email/response.html.twig"

    message: "Merci de votre message ! Je vous réponds dès que possible."
    reset: true

visible: false
body_classes: form
---
